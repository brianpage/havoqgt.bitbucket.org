# CMake generated Testfile for 
# Source directory: /afs/crc.nd.edu/user/b/bpage1/research/havoqgt.bitbucket.org
# Build directory: /afs/crc.nd.edu/user/b/bpage1/research/havoqgt.bitbucket.org/scripts/catalyst.llnl.gov
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("include")
subdirs("src")
subdirs("test")
