#!/bin/bash


INSTALL_PREFIX=${PWD}


rm CMakeCache.txt

cmake ../../ \
  -DHAVOQGT_BUILD_TEST=TRUE \
  -DCMAKE_CXX_COMPILER=/opt/crc/g/gcc/7.1.0/bin/g++ \
  -DCMAKE_C_COMPILER=/opt/crc/g/gcc/7.1.0/bin/gcc \
  -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} \
  -DCMAKE_BUILD_TYPE=Release \
  -DMPI_C_COMPILER=/opt/crc/m/mvapich2/2.2/gcc/7.1.0/mlx-d8civy/bin/mpicc \
  -DMPI_CXX_COMPILER=/opt/crc/m/mvapich2/2.2/gcc/7.1.0/mlx-d8civy/bin/mpic++ \
  -DBOOST_ROOT=/usr/gapps/dst/opt/boost_stages/boost_1_56_0/ \
  -DCMAKE_CXX_FLAGS="-std=c++11 -lrt" \
  #-DCMAKE_CXX_FLAGS="-std=c++11 -DPROCESSES_PER_NODE=16 -DEDGE_PASS_PARTITIONS=4" \
