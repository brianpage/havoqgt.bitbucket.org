# CMake generated Testfile for 
# Source directory: /afs/crc.nd.edu/user/b/bpage1/research/havoqgt.bitbucket.org/test
# Build directory: /afs/crc.nd.edu/user/b/bpage1/research/havoqgt.bitbucket.org/scripts/catalyst.llnl.gov/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(sequential_nompi "test_sequential")
add_test(mpi_communicator_1 "/opt/crc/m/mvapich2/2.2/gcc/7.1.0/mlx-d8civy/bin/mpiexec" "-n" "1" "/afs/crc.nd.edu/user/b/bpage1/research/havoqgt.bitbucket.org/scripts/catalyst.llnl.gov/test/test_mpi_communicator")
add_test(mpi_communicator_2 "/opt/crc/m/mvapich2/2.2/gcc/7.1.0/mlx-d8civy/bin/mpiexec" "-n" "2" "/afs/crc.nd.edu/user/b/bpage1/research/havoqgt.bitbucket.org/scripts/catalyst.llnl.gov/test/test_mpi_communicator")
add_test(mpi_communicator_4 "/opt/crc/m/mvapich2/2.2/gcc/7.1.0/mlx-d8civy/bin/mpiexec" "-n" "4" "/afs/crc.nd.edu/user/b/bpage1/research/havoqgt.bitbucket.org/scripts/catalyst.llnl.gov/test/test_mpi_communicator")
subdirs("gtest")
